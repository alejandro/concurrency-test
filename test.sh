#!/bin/bash
#refs=(4f7cccd b4aa45e 61a8bdf 95fba9d)
refs=(90cc66010114882be7e4e269b852f97f82422c0f 3a8ff899edde8cfd69c6e6633c34ba5e7802d57e 683cd218ddddf7a22dc451a60639fbd855423cc1)
branches=(test)
ref="${refs[$RANDOM % ${#refs[@]}]}:${branches[$RANDOM % ${#branches[@]}]}"
file="results/$(gdate +%s.%N).txt"
echo $ref >> $file
git push -f origin $ref >> $file 2>&1
